﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LoginTest.Entity
{
    public class User : IUser
    {
        private string _id;
        public string Id
        {
            get { return _id; }
        }

        public string UserName
        {
            get;
            set;
        }
    }
}