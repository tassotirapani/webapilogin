﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LoginTest.Entity
{
    public class UserStore : IUserStore<User>
    {
        public async Task CreateAsync(User user)
        {
        }

        public async Task DeleteAsync(User user)
        {
        }

        public async Task<User> FindByIdAsync(string userId)
        {
            var user = new User();
            return user;
        }

        public async Task<User> FindByNameAsync(string userName)
        {
            var user = new User();
            return user;
        }

        public async Task UpdateAsync(User user)
        {
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}